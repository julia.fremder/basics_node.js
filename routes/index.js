const express = require('express');

const path = require('path');

const router = express.Router();
const users = [];

router.get('/', (req, res, next) => {
  res.render('home', { pageTitle: 'Add User', users });
});

router.get('/users', (req, res, next) => {
  res.render('users', { pageTitle: 'Users', users });
});

router.post('/add-user', (req, res, next) => {
  users.push({ name: req.body.username });
  res.redirect('/users');
});

module.exports = router;
