const express = require('express');

const path = require('path');

const myRoutes = require('./routes');

const app = express();

app.set('view engine', 'ejs');
app.use(express.urlencoded({extended: false}))

app.use(express.static(path.join(__dirname, 'public')));

app.use(myRoutes);

app.listen(3030);
